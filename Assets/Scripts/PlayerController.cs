﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float walkSpeed = 1; // player left right walk speed


	Animator animator;

	//some flags to check when certain animations are playing

	
	//animation states - the values in the animator conditions
	const int STATE_IDLE = 0;
	const int STATE_WALK = 1;
	const int STATE_CROUCH = 2;
	const int STATE_JUMP = 3;
	const int STATE_HADOOKEN = 4;
	

	// Use this for initialization
	void Start()
	{

	}

	public void OnAnimationEnd()
	{
		//Game.Instance.OnCutsceneFinished();
		//Game.Instance.NextAction();
	}

	// FixedUpdate is used insead of Update to better handle the physics based jump
	void FixedUpdate()
	{

		//Check for keyboard input
	}
	
	//--------------------------------------
	// Change the players animation state
	//--------------------------------------
	void changeState(int state){
		
	}
	

}