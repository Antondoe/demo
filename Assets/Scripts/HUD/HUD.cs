﻿using System;
using UnityEngine;

public class HUD
{
    HudScreenController ActiveScreenController;

    public event Action onScreenClosed;

    public HUD()
    {
	
    }

    public void  OnScreenClosed()
    {
        var handler = onScreenClosed;
        if (handler != null)
            handler();
    }


    public void ShowWindow(string Name)
    {
        if (ActiveScreenController != null)
        {
            if (ActiveScreenController.CanBeClosed())
            {
                ActiveScreenController.Hide();
            }	
        }

        var screen = SceneConfig.GetCurrentSceneConfig().GetObjectByName(Name) as GameObject;
        if (screen == null)
            return;
        ActiveScreenController = screen.GetComponent<HudScreenController>();

        if (ActiveScreenController != null)
            ActiveScreenController.Show();
    }

    public void CloseWindow(string Name)
    {
        var screen = SceneConfig.GetCurrentSceneConfig().GetObjectByName(Name) as GameObject;
        if (screen == null)
            return;
        var hudController = screen.GetComponent<HudScreenController>();

        if (hudController != null && hudController.CanBeClosed())
        {
            hudController.Hide();
            var handler = onScreenClosed;
            if (handler != null)
                handler();
        }
    }

}

