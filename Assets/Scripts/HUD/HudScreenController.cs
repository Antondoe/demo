﻿using UnityEngine;
using UnityEngine.Experimental.Director;
using System.Collections;

public class HudScreenController : MonoBehaviour
{
    private Vector3 previousPosition;
    public bool Hidden;
    public bool CanBeClosedFlag;
    public Animator animator;

    public AudioClip ShowSound;
    public AudioClip HideSound;

    public AnimationClip HideAnim;
    public AnimationClip ShowAnim;

    private AudioSource source;
    //SetActive is slow
    //TODO:: change it onto something faster and more
    //interesting
    void OnEnable()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayShowSound()
    {
        Game.Instance.PlaySound(ShowSound);
    }

    public void PlayHideSound()
    {
        Game.Instance.PlaySound(HideSound);
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
        Hidden = false;

        PlayAnim(ShowAnim);	
    }

    public virtual bool CanBeClosed()
    {
        return CanBeClosedFlag;
    }

    public virtual void Hide()
    {
        //gameObject.SetActive (false);	
        Game.Instance.HUDManager.OnScreenClosed();
        Hidden = true;
       

        PlayAnim(HideAnim);	
    }

    protected void PlayAnim(AnimationClip clip)
    {
        if (clip == null || animator == null)
            return;
        var clipPlayable = AnimationClipPlayable.Create(clip);
        animator.Play(clipPlayable);
    }
    // Use this for initialization
    void Start()
    {
        gameObject.SetActive(!Hidden);
        source = GetComponent<AudioSource>();
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}

