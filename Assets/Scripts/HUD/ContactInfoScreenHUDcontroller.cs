﻿using System;
using UnityEngine;

using UnityEngine.UI;

public class ContactInfoScreenHUDcontroller:HudScreenController
{

    public InputField Name;
    public InputField lastName;
    public InputField Company;
    public InputField Mail;

    void Start()
    {
        if (Name != null)
        {
            Name.onValueChanged.AddListener(SetName);
            if (Game.Instance.playerInfo.name != "")
                Name.text = Game.Instance.playerInfo.name;
        }

        if (lastName != null)
            lastName.onValueChanged.AddListener(SetLastName);
        if (Company != null)
            Company.onValueChanged.AddListener(SetCompany);
        if (Mail != null)
        {
            Mail.onValueChanged.AddListener(SetEmail);
            if (Game.Instance.playerInfo.contactMail != "")
                Mail.text = Game.Instance.playerInfo.contactMail;
        }
        gameObject.SetActive(!Hidden);
    }

    public override void Hide()
    {
        base.Hide();
        gameObject.SetActive(false);
    }

    public void SetName(string name)
    {
        Game.Instance.playerInfo.name = Name.text;
    }

    public void SetLastName(string name)
    {
        Game.Instance.playerInfo.lastName = lastName.text;
    }

    public void SetEmail(string email)
    {
        Game.Instance.playerInfo.contactMail = Mail.text;		
    }

    public void SetCompany(string company)
    {
        Game.Instance.playerInfo.company = Company.text;
    }

}

