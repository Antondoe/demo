﻿//
//  WhitePagesWidthScaler.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2017 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using UnityEngine;

public class WhitePagesWidthScaler : MonoBehaviour
{
	public 	RectTransform Rect;
	public Camera cam;

	void Start ()
	{
		if (Rect != null && cam != null) {
			float height = Camera.main.orthographicSize * 2.0f;
			float width = height * Screen.width / Screen.height;
			Debug.Log (width);
			Rect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, Screen.width * width / Screen.width / Rect.localScale.x);
			//Rect.localScale = new Vector3 (width / Screen.width, Rect.localScale.y, Rect.localScale.z);
		}
	}
}




