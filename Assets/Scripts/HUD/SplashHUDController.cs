﻿//
//  SplashHUD.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using UnityEngine;


public class SplashHUDController: HudScreenController
{
    private float safetime = 0;

    void Update()
    {
        if (safetime < 2 && Hidden == false)
        {
            safetime += Time.deltaTime;
            return;
        }
        if (Input.anyKey && !Hidden)
        {
            Hide();
            Hidden = true;
        }
    }

 
    public override void Hide()
    {
        //TODO: move playanimation to base class
        base.Hide();
        Game.Instance.HUDManager.OnScreenClosed();
        //transform.parent = Game.Instance.GameCamera.transform;
    }


}


