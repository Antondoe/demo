﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct AliasRecord
{
	[SerializeField]
	public String Name;
	[SerializeField]
	public GameObject SceneObject;

	public AliasRecord (string name, GameObject obj)
	{
		this.Name = name;
		SceneObject = obj;
	}
}

[Serializable]
public class SceneConfig: MonoBehaviour
{
	public List<AliasRecord> VisualScriptObjects;
	private Dictionary<string,UnityEngine.Object> map = new Dictionary<string,UnityEngine.Object> ();

	public static SceneConfig GetCurrentSceneConfig ()
	{
		if (Game.Instance != null)
			return Game.Instance.CurrentConfig;
		return null;
	}

	public UnityEngine.Object GetObjectByName (string name)
	{
		UnityEngine.Object someObj = null;
		map.TryGetValue (name.ToLower (), out someObj);
		return someObj;
	}

	void OnEnable ()
	{
		if (VisualScriptObjects == null)
			VisualScriptObjects = new List<AliasRecord> ();

		foreach (var item in VisualScriptObjects) {
			map.Add (item.Name.ToLower (), item.SceneObject);
		}

		Game.Instance.CurrentConfig = this;

		hideFlags = HideFlags.HideInInspector & HideFlags.HideInHierarchy;
	}
}

