﻿using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

[CustomEditor (typeof(SceneConfig))]
public class SceneConfigEditor : EditorWindow
{
	protected SceneConfig sceneConfig;

	[MenuItem ("Window/Scene Config")]
	public static void Open ()
	{
		SceneConfigEditor w = GetWindow<SceneConfigEditor> (); 

		w.sceneConfig = FindObjectOfType<SceneConfig> ();
		if (w.sceneConfig == null) {
			var obj = new GameObject ();
			obj.hideFlags = HideFlags.HideInHierarchy;
			w.sceneConfig = obj.AddComponent<SceneConfig> ();
		}

	}

	void CreateConfig ()
	{
	}

	public  void OnGUI ()
	{

		var m_Object = new SerializedObject (sceneConfig);
		var m_Property = m_Object.FindProperty ("VisualScriptObjects");
		EditorGUILayout.PropertyField (m_Property, new GUIContent ("Scene Objects"), true);
		m_Object.ApplyModifiedProperties ();
		//EditorGUI.PropertyField(new Rect(0,0,100,20),sceneConfig.	
		//var myAsset = serializedObject.FindProperty("MemberAsset");
		//CreateCachedEditor(sceneConfig, null, ref this);
		//_editor.OnInspectorGUI();

		//serializedObject.ApplyModifiedProperties();
	}
}	