﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void FOnCutsceneFinished();

public struct PlayerInfo
{
    public string contactMail;
    public string name;
    public string lastName;
    public string company;

    public void Clear()
    {
        contactMail = "";
        name = "";
        lastName = "";
        company = "";
    }

    public override string ToString()
    {
        return "Company: " + company +
        "\n E-Mail: " + contactMail +
        "\n Name: " + name +
        "\n Last Name:  " + lastName;
    }
}

public class Game : MonoBehaviour
{
    private static Game _instance;
    private WebRequestsProcessor requestProcessor;

    public HUD HUDManager = new HUD();
    public PlayerInfo playerInfo;
    public int pixels;
    public ScriptContainer Dialog;

    public DialogNodeProxy NpcTextArea;
    public DialogNodeProxy PlayerTextArea;
    public DialogNodeProxy SubtitlesTextArea;
    public Camera GameCamera;
    public RectTransform SafeFrame;
    public bool bActionCompleated = false;
    public float TextOutSpeed = 0.5f;
    public Transform GameCharacter;
    public string log = "";

    private IDictionary SceneObjects;

    [NonSerialized]
    public SceneConfig CurrentConfig;

    private bool quitBool;
    private AudioSource source;

    public void PlaySound(AudioClip sound)
    {
        if (source != null)
            source.PlayOneShot(sound, 0.7f);
    }

    public void AddToLog(string Text)
    {
        log += Text + "\n";
    }

    public void SendMail(string Adress, string Subject, string Text)
    {
        requestProcessor.AddSendMailRequest(Adress, Subject, Text);
    }

    private float MatchCameraToSafeFrames()
    {
        //SafeFrame.bounds.size;
        Debug.Log("Matching a camera");
        var cam = GameCamera.GetComponent <Camera>();
        var SFScale = SafeFrame.transform.localScale;

        Vector2 FrameSize = new Vector2(SafeFrame.rect.width * SFScale.x, SafeFrame.rect.height * SFScale.y);

        float SFA = SFScale.x / SFScale.y;
        float a = 1 / SFA / cam.aspect;

        if (SFA / 2 / cam.aspect > 1)
            a = FrameSize.x / cam.rect.width / cam.aspect / 2;
        else
            a = FrameSize.y / 2;

        //a = FrameSize.y / 2;
        //a = FrameSize.x / cam.rect.width / cam.aspect / 2;
	
        cam.orthographicSize = a;  
        return a;
    }

    public void OnEnable()
    {
        if (!_instance)
        {	
            _instance = this;
            /*
            playerInfo.name = "interviewer";
            playerInfo.company = "unknown company";
            playerInfo.contactMail = "unknown mail";
            playerInfo.lastName = "interviewer";
            */
            playerInfo.Clear();
            source = GetComponent<AudioSource>();
            requestProcessor = gameObject.AddComponent<WebRequestsProcessor>();
        }

        MatchCameraToSafeFrames();
    }

    public static Game Instance
    {
        get { return _instance; }
		
    }


    bool isFirstStart()
    {
        return true;
    }

    void StartDialog()
    {
        if (Dialog != null)
        {
            Dialog.Exec();
        }	

    }

    void Start()
    {
        StartDialog();
        //MatchCameraToSafeFrames ();
        return;
        //instance = this;
        if (isFirstStart())
        {
            HUDManager.ShowWindow("WelcomeScreen");
        }

    }

    /*
	public static void GetNext(int Index)
	{

		if (_instance.CurrentNode.Childs.Length == 0 ) _instance.EndDialog();
		if (Index < _instance.CurrentNode.Childs.Length)
		{
			_instance.CurrentNode = _instance.CurrentNode.Childs[Index];
			_instance.Render();
		}
	}
	*/

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 1)
            quitBool = false;
        if (Input.GetKeyDown(KeyCode.Escape) && quitBool == true)
        {
            Application.Quit();
        }
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.Escape))
                quitBool = true;
            else
                quitBool = false;
        }
		
    }
}
