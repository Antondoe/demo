﻿//
//  GameScript.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using UnityEngine.Experimental.Director;
using UnityEngine;

public class GameScriptProcessor:ScriptProcessor
{
    public event Action OnScreenClosedEvent;

    public GameScriptProcessor(ScriptContainer container)
        : base(container)
    {
		
        ExposeMethod("CallWait");
        ExposeMethod("ShowWindow");
        ExposeMethod("SendContactData");
        ExposeMethod("SendLog");
        ExposeMethod("OpenURL");
        ExposeMethod("PlaySound");
        ExposeMethod("TestConnection");
        ExposeEvent("OnScreenClosedEvent");

    }

    public override void OnStart()
    {
        //To prevent multiply bindings
        Game.Instance.HUDManager.onScreenClosed -= OnScreenClosed;
        Game.Instance.HUDManager.onScreenClosed += OnScreenClosed;
    }

    #region Exposed methods and events

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    public void PlaySound(AudioClip clip)
    {
        Game.Instance.PlaySound(clip);
    }

    public void OnScreenClosed()
    {
        var h = OnScreenClosedEvent;
        if (h != null)
        {
            h();
            Debug.Log("screen closed");
        }
    }

    public void TestConnection()
    {

        if (Application.internetReachability != NetworkReachability.NotReachable)
            Script.UpdateCondition(new Condition("ConnectedToInternet true"));
        else
            Script.UpdateCondition(new Condition("ConnectedToInternet false"));
       
    }

    public void SendLog()
    {
        string log = "User: \n " + Game.Instance.playerInfo.ToString() + "\n";
        Game.Instance.SendMail("gordon20003@yandex.ru", "Insider Log", log + "\n" + Game.Instance.log);
        Debug.Log("Log were sent");    
    }

    public void SendContactData()
    {
        string data = "Anton Trufanov\nGame Programmer\n\nContacts:\ne-mail: anton.doe@yandex.com\ne-mail: wiresandflowers@gmail.com\nSkype: anton.doe1\n\nBlog:\nantondoe.blogspot.com";
        data += "\n\nThank you for that you have found a time to check my App. I hope, you enjoyed it.";
        Game.Instance.SendMail(Game.Instance.playerInfo.contactMail, "Contacts", data);
        Debug.Log("Contacts were sent @ " + Game.Instance.playerInfo.contactMail);
    }

    public void ShowWindow(string Name)
    {
        Game.Instance.HUDManager.ShowWindow(Name);
        Debug.Log("show " + Name);
    }

    public void AddRespect(int Respect)
    {
        return;	
    }

    protected  void RenderSwitchNode(SwitchDialogFlowNode node)
    {
        var OutPin = Script.GetNodePins(node).Find(p => p.PinType == EPinType.EPinOut);
        if (OutPin != null)
        {
            var nodes = Script.GetConnectedNodes(node, OutPin);
            switch (node.SwitchType)
            {

                case ESwitchType.UserSelected:
                    {
                        if (node.SpeakerType == ESpeakerType.Player)
                            RenderDialogNodes(nodes);
                         
                        break;
                    }

                case ESwitchType.Sequientional:
                    {
                        RenderDialogNode(nodes.Find(p => p.IsNodeEnabled()) as BaseDialogNode);
                        break;
                    }

                case ESwitchType.Random:
                    {
                        break;}
                default:
                    break;
            }
        }
    }

    #endregion

    protected override void ProcessNode(BaseNode node)
    {
        var CurrentDialogNode = node;
        if (CurrentDialogNode is SwitchDialogFlowNode)
        {
            RenderSwitchNode(CurrentDialogNode as SwitchDialogFlowNode);
            return;
        }

        if (CurrentDialogNode is BaseDialogNode)
        {
            RenderDialogNode(CurrentDialogNode as BaseDialogNode);
            return;
        }

        if (node != null)
            node.Exec(ProcessNode);

    }

    protected void RenderDialogNode(BaseDialogNode node)
    {
        List<BaseNode> list = new List<BaseNode>();
        list.Add(node);
        RenderDialogNodes(list);
    }

    protected void RenderDialogNodes(List<BaseNode> nodes)
    {
        var gameInstance = Game.Instance;
        gameInstance.NpcTextArea.Reset();
        gameInstance.PlayerTextArea.Reset();
        gameInstance.SubtitlesTextArea.Reset();

        foreach (var item in nodes)
        {
            var _item = item as BaseDialogNode;
            if (_item == null || _item.IsNodeEnabled() == false)
                continue;

            switch (_item.GetSpeakerType())
            {
				
                case ESpeakerType.NPC:
                    {
                        gameInstance.NpcTextArea.AddNode(_item);
                        break;
                    }

                case ESpeakerType.Subtitles:
                    {
                        gameInstance.SubtitlesTextArea.AddNode(_item);
                        break;	
                    }
                case ESpeakerType.Player:
                    {
                        gameInstance.PlayerTextArea.AddNode(_item);
                        break;
                    }
				
                default:
                    break;
            }
        }
		

        gameInstance.SubtitlesTextArea.DrawNodesView(ProcessNode);
        gameInstance.NpcTextArea.DrawNodesView(ProcessNode);
        gameInstance.PlayerTextArea.DrawNodesView(ProcessNode);

    }

}

