﻿using UnityEngine;
using System.Collections;

public class NPCDialogProxy : DialogNodeProxy
{

    public int MaxSpeechBoxLines = 3;
    public int MaxSpeechBoxLineLength = 50;
    public Vector2 margins = new Vector2(0.1f, 0f);
    private Vector2 CharSize;
    private float HandleHeight;
    private Vector2 BodySize;
    public Transform SpeechBoxBody;
    public Transform SpeechBoxHandle;

    // Use this for initialization


    public void Start()
    {

        base.Start();

        var SpeechBoxHandleMeshRenderer = SpeechBoxHandle.gameObject.GetComponent<SpriteRenderer>();
        var SpeechBoxBodyMeshRenderer = SpeechBoxBody.gameObject.GetComponent<SpriteRenderer>();

        if (SpeechBoxHandleMeshRenderer != null)
            HandleHeight = SpeechBoxHandleMeshRenderer.bounds.size.y;

        if (SpeechBoxBodyMeshRenderer != null)
            BodySize = SpeechBoxBodyMeshRenderer.bounds.size;
    }

    /*
	public override void DrawNode ()
	{
		SetSpeechBoxEnabled (true);

		base.DrawNode ();
	}
	*/
    protected override void PrintText(string Text)
    {
        SetSpeechBoxEnabled(true);
        DrawSpeechBox(Text);
        base.PrintText(Text);
    }

    public override void DrawNodesView(System.Action<BaseNode> callback)
    {
        base.DrawNodesView(callback);
    }

    void SetSpeechBoxEnabled(bool bEnabled)
    {
		
        var SpeechBoxBodyMeshRenderer = SpeechBoxBody.gameObject.GetComponent<SpriteRenderer>();
        var SpeechBoxHandleMeshRenderer = SpeechBoxHandle.gameObject.GetComponent<SpriteRenderer>();

        SpeechBoxBodyMeshRenderer.enabled = bEnabled;
        SpeechBoxHandleMeshRenderer.enabled = bEnabled;

    }

    /*
	protected override void PrintText (string OutText)
	{
		base.PrintText (OutText);
		DrawSpeechBox (OutText);
	}
	*/
    //return count of characters that should be splitted ?
    int DrawSpeechBox(string Text)
    {
        SetSpeechBoxEnabled(true);
        int NumLines = Mathf.CeilToInt(Text.Length / MaxSpeechBoxLines);
        int reminder = Text.Length - MaxSpeechBoxLines * NumLines;
        var txtSize = GetTextBounds(Text).size;
        DrawAreaBounds(GetTextBounds(Text));
        Vector3 scale = new Vector3(txtSize.x / BodySize.x + margins.x, txtSize.y / BodySize.y + margins.y, 1);
        SpeechBoxBody.localScale = scale;
        var yOffet = SpeechBoxBody.GetComponent<Renderer>().bounds.size.y;
        SpeechBoxHandle.position = SpeechBoxBody.position - new Vector3(0, yOffet / 2, 0);		
	
        return reminder;
    }

    Bounds GetTextBounds(string text)
    {
        TextTemplate.text = text;
        var res = TextTemplate.GetComponent<Renderer>().bounds;
        TextTemplate.text = "";
        return res;	
    }

    protected override void OnSkip()
    {

        if (Nodes.Count > 0 && blockSelection <= 0)
        {
            var node = Nodes[0];
            SetSpeechBoxEnabled(false);
            Reset();
            SelectNode(node);
        }
    }

    protected override void OnClick(int Index)
    {
        OnSkip();
        return;
        //Game.Instance.PlayAction (Nodes [Index]);
    }


}
