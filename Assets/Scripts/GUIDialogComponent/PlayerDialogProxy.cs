﻿using UnityEngine;

using System.Collections;

public class PlayerDialogProxy : DialogNodeProxy
{

    public SpriteRenderer SelectionRowTemplate;
    public Vector2 margins = new Vector2(0.3f, 0.3f);
    private bool bSelectionStarted = false;

    IEnumerator PlaySelectionAnimation(int Index)
    {
        SelectionRowTemplate.enabled = true;
        //set it in a right place
        var pos = Instances[Index].position;
        var selfpos = SelectionRowTemplate.transform.position;
        var txtRenderer = Instances[Index].gameObject.GetComponent<MeshRenderer>();
        //txtRenderer.transform.position += new Vector3(0.2f,0.0f,0.0f);
        Vector3 txtSize = txtRenderer.bounds.size;
        Vector3 selectionSize = SelectionRowTemplate.bounds.size;
        Vector3 scale = new Vector3(txtSize.x / selectionSize.x + margins.x, txtSize.y / selectionSize.y, 1);

        SelectionRowTemplate.transform.position = new Vector3(pos.x + txtSize.x / 2, pos.y, selfpos.z);

        SelectionRowTemplate.transform.localScale = scale;


        yield return new WaitForSeconds(0.5f);

        SelectionRowTemplate.enabled = false;
        SelectionRowTemplate.transform.localScale = new Vector3(1, 1, 1);
        //SelectNode (Index);
	
        base.OnClick(Index);
        bSelectionStarted = false;
        //Show selection frame for 2 seconds
        //hide it
        //call delegate
    }

    // Use this for initialization
    public void Start()
    {
	
        base.Start();
    }



    protected override void OnClick(int Index)
    {
        foreach (var item in Instances)
        {
            var ctm = item.gameObject.GetComponent<CustomTextMesh>();
            if (ctm != null)
            {
                ctm.ForceOut();
            }
        }   
        if (Index >= 0 && Index < Nodes.Count && bSelectionStarted == false)
        {
            StartCoroutine(PlaySelectionAnimation(Index));
            bSelectionStarted = true;
        }
    }

    void OnAnimationPlayed(BaseDialogNode Node)
    {
        //Game.Instance.PlayAction (Node);

    }

    protected override void OnSkip()
    {
        return;
    }
    // Update is called once per frame
    void Update()
    {
        base.Update();
    }
}
