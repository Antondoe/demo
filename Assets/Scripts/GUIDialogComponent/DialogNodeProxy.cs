﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class DialogNodeProxy : MonoBehaviour
{

    public TextMesh TextTemplate;
    public AudioClip Sound;
    private AudioSource source;

    protected float RelativeLineHeight;
    protected List<RectTransform> OwnedBranches = new List<RectTransform>();
    protected List<Bounds> Areas = new List<Bounds>();

    protected List<BaseDialogNode> Nodes = new List<BaseDialogNode>();
    protected List<Transform> Instances = new List<Transform>();

    protected Vector3 translate = new Vector3();
    protected int CurrentIndex;
    protected Action<BaseNode> callback;

    protected float blockSelection;
    protected CustomTextMesh CTM;

    protected virtual void DeleteInstances()
    {
        foreach (var item in Instances)
        {
            Destroy(item.gameObject);
        }
        Instances.Clear();
    }

    void OnEnable()
    {
        source = GetComponent<AudioSource>();
    }

    public virtual void AddNode(BaseDialogNode node)
    {
        Nodes.Add(node);
    }

    public void Reset()
    {
        CurrentIndex = 0;
        DeleteInstances();
        Nodes.Clear();
        translate = new Vector3();
        blockSelection = 0.2f;
    }


    protected virtual void PrintText(string Text)
    {
      
        Game.Instance.PlaySound(Sound); 
        return;
    }

    public virtual void DrawNodesView(Action<BaseNode> callback)
    {
        if (Nodes == null || Nodes.Count == 0)
            return;
        TextTemplate.text = "";
        var TemplateHeight = TextTemplate.GetComponent<Renderer>().bounds.size.y;
        string text = "";
        TextTemplate.text = "";
        Action NextRenderer = null;
        TextMesh instance;
        CustomTextMesh ctm = null;
        foreach (var item in Nodes)
        {
          
            instance = TextMesh.Instantiate(TextTemplate, TextTemplate.transform.parent) as TextMesh;
            Instances.Add(instance.transform);
            TextTemplate.text = text;

            var size = TextTemplate.GetComponent<Renderer>().bounds.size;

            var dialogText = item.GetDialogText();
            dialogText = dialogText.Replace("%username%", Game.Instance.playerInfo.name);

            var lastCtm = ctm; 
            ctm = instance.GetComponent<CustomTextMesh>();
            if (ctm != null)
            {
                dialogText = ctm.GetALignedText(dialogText);
                NextRenderer = new Action(ctm.PrintText);
                if (lastCtm != null)
                    lastCtm.callback = NextRenderer;
            }
            text += dialogText + "\n\n";


            instance.text = dialogText;
            var newPosition = instance.transform.position + new Vector3(0, -size.y + TemplateHeight, 0); 
            instance.transform.position = newPosition;
            instance.text = "";
            ctm.Init(dialogText);
            
           
        }

        ctm = Instances[0].gameObject.GetComponent<CustomTextMesh>();

        if (ctm != null)
            ctm.PrintText();

        TextTemplate.text = "";
        PrintText(text);


        this.callback = callback;
    }


    protected void DrawAreaBounds(Bounds area)
    {
        Vector3 p1 = area.center - area.extents;
        Vector3 p2 = p1 + new Vector3(area.size.x, 0, 0);
        Vector3 p3 = p2 + new Vector3(0, area.size.y, 0);
        Vector3 p4 = p3 + new Vector3(-area.size.x, 0, 0);

        Debug.DrawLine(p1, p2, Color.red, 100);
        Debug.DrawLine(p2, p3, Color.red, 100);
        Debug.DrawLine(p3, p4, Color.red, 100);
        Debug.DrawLine(p1, p4, Color.red, 100);

    }

    protected void SelectNode(BaseNode node)
    {
        Reset();
        var dialogNode = node as BaseDialogNode;
        if (dialogNode != null)
            Game.Instance.AddToLog(dialogNode.GetSpeakerType().ToString() + ": " + dialogNode.GetDialogText());

        if (callback != null)
            node.Exec(callback);
    }

    protected virtual void Start()
    {
        TextTemplate.text = "";
    }

    protected virtual void OnClick(int Index)
    {
        if (Index >= 0 && Index < Nodes.Count)
        {
            {

                SelectNode(Nodes[Index]);

            }
        }

    }

    protected virtual void OnSkip()
    {
        //SelectNode (0);
    }

    // Update is called once per frame
    protected void Update()
    {
        if (blockSelection > 0)
        {
            blockSelection -= Time.deltaTime;
            return;
        }

        if (Input.GetMouseButtonUp(0))
        {
            int Index = 0;
            foreach (var item in Instances)
            {
                Ray vRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (item.GetComponent<MeshRenderer>().bounds.IntersectRay(vRay))
                {
                    if (Nodes.Count > 0)
                    {
                        OnClick(Index);
                        //Game.Instance.PlayAction(Nodes[Index]);
                        return;
                    }
                    continue;
                }
                Index++;
            }

            OnSkip();
        }



    }
}
