﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CustomTextMesh : MonoBehaviour
{
    public int MaxLineWidth = 30;
    private List<string> words = new List<string>();
    private int wordsCounter;
    private int lineWidth;
    private TextMesh textMesh;
    private float delay;
    //it'ś bad to do that public;
    public Action callback;
    private string textToOut;

    public void PrintText()
    {
        words = new List<string>(textToOut.Split(' '));
        textMesh.text = "";
        lineWidth = MaxLineWidth;
        wordsCounter = words.Count;
        delay = 0;
    }

    public void Init(string text)
    {
        textToOut = text;
        // this.callback = callback;
    }

    public string GetALignedText(string text)
    {
        string res = string.Empty;
        textMesh = GetComponent<TextMesh>();
        words = new List<string>(text.Split(' '));
        lineWidth = MaxLineWidth;
        while (words.Count > 0)
        {
            string word = words[0];
            lineWidth -= word.Length;
            if (lineWidth < 0)
            {
                lineWidth = MaxLineWidth - word.Length - 1;
                res = res.TrimEnd(' ');
                word = "\n" + word + " ";
                res += word;
                words.RemoveAt(0);
                continue;
            }
            res += word + " ";
            words.RemoveAt(0);
            lineWidth--;
        }
        res = res.TrimEnd(' ');
        return res;
    }

    void Update()
    {
        if (words.Count > 0)
        {
            if (delay <= 0)
            {
                string word = words[0];
                words.RemoveAt(0);
                if (words.Count > 0)
                    word += " ";
                textMesh.text += word;
                delay = Game.Instance.TextOutSpeed;
                if (words.Count == 0)
                if (callback != null)
                    callback();
            } 
            delay -= Time.deltaTime;
        }
    }

    public void ForceOut()
    {
        words.Clear();
        textMesh.text = textToOut;
    }
}

