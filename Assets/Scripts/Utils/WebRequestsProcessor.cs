﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;


public class WebRequestsProcessor : MonoBehaviour
{

    //Unsafe class

    #region Inner class for thread

    private Queue<MailMessage> mailRequests = new Queue<MailMessage>();

    private class RequestProcessor
    {
        //public object locker;
        public bool ready = true;
        private bool bShutDown = false;
        private MailMessage mailMessage;
        private SmtpClient smtpServer = new SmtpClient();
        public string ResultString = string.Empty;

        public void Finish()
        {
            bShutDown = true;
        }

        private void InitSMTPServer()
        {
            smtpServer = new SmtpClient();
            smtpServer.Host = "smtp.yandex.com";
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("anton.doe@yandex.com", "weakpassw0rd") as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            ServicePointManager.ServerCertificateValidationCallback = 
                delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
        }

        /// <summary>
        /// That was totally bad idea
        /// I must use php script for sending mails
        /// </summary>
        public void Start()
        {
            Idle();    
        }

        public void SendMail(MailMessage mail)
        {
            mailMessage = new MailMessage();
            mailMessage = mail; 
            ready = false;
        }

        void Idle()
        {
            while (!bShutDown)
            {
                if (ready == false)
                {
                    try
                    {
                        //ResultString = String.Empty;
                        InitSMTPServer();
                        smtpServer.Send(mailMessage);
                        ResultString += ("Mail was sent" + mailMessage.To + "\n");
                        mailMessage.Dispose();
                        mailMessage = null;
                        ready = true;
                    }
                    catch (Exception ex)
                    {
                        ResultString += ex.Message + "\n";
                        mailMessage = null;
                        ready = true;
                        //return;
                    }    
                }
            }
        }

    }

    #endregion

    Thread WorkingThread;

    void Update()
    {
        if (WorkingThread != null && WorkingThread.IsAlive)
        {
            if (mailRequests.Count > 0 && requestProcessor.ready)
            {
                requestProcessor.SendMail(mailRequests.Dequeue());
            }
        }
    }

    void OnGUI()
    {

        //GUI.Label(new Rect(0, 0, 300, 40), requestProcessor.ResultString);
    }

    private RequestProcessor requestProcessor = new RequestProcessor();

    void Start()
    {
        WorkingThread = new Thread(requestProcessor.Start);
        WorkingThread.Start();
    }

    void OnDestroy()
    {
        requestProcessor.Finish();
        WorkingThread.Join();
    }

    public void AddSendMailRequest(string email, string subject, string text)
    {
        
        if (subject == null || text == null || email == null)
            return;
        MailMessage mail = new MailMessage();
			
        mail.From = new MailAddress("anton.doe@yandex.com");
        mail.To.Add(email);
        mail.Subject = subject;
        mail.Body = text;
        mailRequests.Enqueue(mail);
        //requestProcessor.SendMail(mail);            
         
    }
}