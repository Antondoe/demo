﻿using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using System.Linq;



namespace EditorClasses
{
	public class Selection
	{
		#if UNITY_EDITOR
		public
		List<IEditorVisualElement> selection = new List<IEditorVisualElement> ();

		private Rect SelectionRect;
		private bool bDrawRect = false;
		private bool bSelectionFinished = false;

		public bool IsAnyObjectUnderMouseCursor (Vector2 mouseCoors)
		{
			foreach (var item in selection) {
				if (item.IsUnderMouseCursor (mouseCoors))
					return true;
			}

			return false;
		}

		public void SetRectCornerA (Vector2 mouseCoords)
		{
			bDrawRect = true;
			SelectionRect.xMin = mouseCoords.x;
			SelectionRect.yMin = mouseCoords.y;
			DrawSelectionRect ();

		}

		public void SetRectCornerB (Vector2 mouseCoords)
		{
			bDrawRect = true;
			SelectionRect.xMax = mouseCoords.x;
			SelectionRect.yMax = mouseCoords.y;
			DrawSelectionRect ();
		}

		public IEditorVisualElement GetItemUnderCursor (Vector2 MouseCoords)
		{
			foreach (var item in selection) {
				if (item.IsUnderMouseCursor (MouseCoords))
					return item;
			}	
			return null;
		}

		public bool IsInSelection<T> (IEditorVisualElement someObejct)
		{
			if (selection.Contains (someObejct)) {
				if (someObejct is T)
					return true;
			} 

			return false;
		}

		public void DrawSelectionRect ()
		{
		
			//Debug.Log (r);
			if (bDrawRect)
				GUI.Box (SelectionRect, "");
			//Repaint ();
		}


		public bool Select (List<IEditorVisualElement> objects, Vector2 MouseCoords)
		{
			selection = objects.Where (p => {
				bool bSelected = p.IsUnderMouseCursor (MouseCoords);
				p.Select (bSelected);
				return bSelected;
			}).ToList ();
			bSelectionFinished = selection.Count > 0 ? true : false;
			return bSelectionFinished;
		}

		public bool Select (List<IEditorVisualElement> objects)
		{
			selection = objects.Where (p => {
				bool bSelected = p.IsInSelectionRect (SelectionRect);
				p.Select (bSelected);
				return bSelected;
			}).ToList ();
			bDrawRect = false;
			bSelectionFinished = selection.Count > 0 ? true : false;
			return bSelectionFinished;
		}

		public void DeleteObjects ()
		{
			for (int i = 0; i < selection.Count; i++) {
				selection [i].Delete ();
				selection.RemoveAt (i);
				i--;
			}
			bSelectionFinished = false;
			
						
		}

		public void Drag (Vector2 delta)
		{
			foreach (var item in selection) {
				item.Drag (delta);
			}
		}
		#endif
	}
}

