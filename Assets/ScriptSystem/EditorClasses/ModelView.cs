﻿//
//  Model.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Linq;
using EditorClasses;

public class ModelView
{
    #if UNITY_EDITOR
    private bool bDrawSelectionRect = false;
    public EditorClasses.Selection selection;
    private readonly ScriptContainer Data;
    public List<IEditorVisualElement> View = new List<IEditorVisualElement>();

    //-----draw link---------------
    private bool bCreateLink = false;
    private bool bInDragMode = false;
    private TPin StartPin;
    private Vector2 EndCoords;
    float normalThickness = 1;
    //----------------------------

    private Vector2 PanningDelta;


    public ModelView(ScriptContainer container)
    {
        selection = new EditorClasses.Selection();
        Data = container;
    }

    private void DrawLink()
    {
        TLink.DrawLink(StartPin.PinArea.center, EndCoords, StartPin.Tangent, normalThickness);
    }

    public void Pan(Vector2 delta)
    {
        PanningDelta = delta;
    }

    public void OnDrag(Vector2 MouseCoords, Vector2 delta)
    {
        if (bCreateLink)
        {
            EndCoords = MouseCoords;
            return;
        }

        if (selection.IsAnyObjectUnderMouseCursor(MouseCoords) || bInDragMode)
        {
            selection.Drag(delta);
            bInDragMode = true;
        }
        else
        {
            selection.SetRectCornerB(MouseCoords);
            bDrawSelectionRect = true;
        }
    }

    public void OnMouseDown(Vector2 MouseCoords, int button)
    {
        if (button == 0)
        {
            if (selection.IsAnyObjectUnderMouseCursor(MouseCoords))
            {
                //do drag
                return;	
            }
            else
            {
                if (selection.Select(View, MouseCoords))
                {
                    var pin = selection.selection.Find(p => p is TPin) as TPin;
                    if (pin != null)
                    {
                        //
                        StartPin = pin;
                        bCreateLink = true;
                    }
                    //do drag
                    return;
                }
                else
                {
                    selection.SetRectCornerA(MouseCoords);
                    selection.SetRectCornerB(MouseCoords);
                }
            }
        }
    }

    public void XorSelection(Vector2 MouseCoords)
    {

    }

    public void OnMouseUp(Vector2 MouseCoords, int button)
    {
        bInDragMode = false;
        if (bDrawSelectionRect)
        {
            selection.Select(View);
            bDrawSelectionRect = false;
        }

        if (bCreateLink)
        {
            selection.Select(View, MouseCoords);
            var EndPin = selection.selection.Find(p => p is TPin) as TPin;
            if (EndPin != null)
            {
                Data.LinkNodes(StartPin, EndPin);
            }
            //CreateLink
            bCreateLink = false;
        }
			
    }

    IEditorVisualElement GetItemUnderCursor(Vector2 MouseCoords)
    {
        foreach (var item in View)
        {
            if (item.IsUnderMouseCursor(MouseCoords))
                return item;
        }
        return null;
    }

    public void Draw()
    {

        int index = 0;

        foreach (var item in View)
        {
            item.Draw(index++, PanningDelta);
        }
        if (bDrawSelectionRect)
            selection.DrawSelectionRect();
        if (bCreateLink)
            DrawLink();
        PanningDelta = new Vector2(0, 0);

    }

    public void DeleteSelected()
    {
        selection.DeleteObjects();
        Data.UpdateModelView();
    }
    #endif
}


