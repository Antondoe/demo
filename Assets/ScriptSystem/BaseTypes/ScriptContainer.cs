using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using TypeReferences;
using UnityEngine;
using System.Linq;

#region helpers
[Serializable]
public class TLinks
{
    [SerializeField]
    public List<TLink> links = new List<TLink>();

    public TPin[] GetPins(TLink link, ref List<TPin> pins)
    {
        TPin[] outpins = new TPin[2];
        pins[0] = outpins[link.Pin1Id];
        pins[1] = outpins[link.Pin2Id];
        return outpins;
    }

    public List<TLink> GetLinks(TPin Pin)
    {
        return links.Where(p => (p.Pin1 == Pin || p.Pin2 == Pin)).ToList();
    }

    public bool IsLinkExists(TPin A, TPin B)
    {
        foreach (var item in links)
        {
            if (item.Pin1 == A && item.Pin2 == B)
                return true;
            if (item.Pin1 == B && item.Pin2 == A)
                return true;
        }
        return false;
    }

    public bool AddLink(TLink newLink)
    {
        if (IsLinkExists(newLink.Pin2, newLink.Pin1))
            return false;
        else
        {
            links.Add(newLink);
            return true;
        }
    }

    public bool DeleteLink(TLink link)
    {
        return links.Remove(link);	
    }
}

//TPin Knows about owner node
[Serializable]
public class TPins
{
    [SerializeField]
    public List<TPin> pins = new List<TPin>();

    public void Add(TPin pin)
    {
        pins.Add(pin);
    }

    public void Add(List<TPin> pinsToAdd)
    {
        pins.AddRange(pinsToAdd);
    }

    public List<BaseNode> GetConnectedNodes(TNodes nodes, TPin pin, TLinks links)
    {
        var selectedPins = GetLinks(pin, links).Select(p =>
            {
                if (p.Pin1 != pin)
                    return p.Pin1;
                else
                    return  p.Pin2; 
            }).ToList();

        return selectedPins.Select(p => p.OwnerNode).ToList();
    }

    public int GetPinIndex(TPin Pin)
    {
        return pins.FindIndex(p => p == Pin);
    }

    public List<TPin> GetPins(BaseNode node)
    {
        return pins.Where(p => p.OwnerNode == node).ToList();
    }

    bool DeleteLink(TLink link, TLinks links)
    {
        return links.DeleteLink(link);
    }

    public List<TLink> GetLinks(TPin pin, TLinks links)
    {
        return links.GetLinks(pin);
    }

    public bool DeletePin(TPin pin)
    {
        return pins.Remove(pin);
    }
}

[Serializable]
public class TNodes
{
    [SerializeField]
    public List <BaseNode> nodes = new List<BaseNode>();

    public T CreateNode<T>(ref TPins pins, TNodeComponents variables) where T : BaseNode
    {
        T node = BaseNode.CreateInstance<T>();
        var GeneratedPins = node.GeneratePins();
        pins.Add(GeneratedPins);
        nodes.Add(node);

        return node;
    }

    public List<BaseNode> FindNode <T>() where T :BaseNode
    {
        return nodes.Where(p => p is T).ToList();
    }

    public void AddNode(BaseNode node)
    {
        nodes.Add(node);
    }

    public List<TPin> GetPins(BaseNode node, TPins pins)
    {
        return pins.GetPins(node);	
    }

    public List<TPin> GetPins(BaseNode node, TPins pins, EPinType pinType)
    {
        return GetPins(node, pins).Where(p => p.PinType == pinType).ToList();
    }

    public List<BaseNode> GetConnectedNodes(BaseNode node, TPins pins, TLinks links, TPin pin)
    {
        return pins.GetConnectedNodes(this, pin, links);
    }

    List<TLink> GetLinks(BaseNode node, TPins pins, TLinks links)
    {
        return pins.GetPins(node).SelectMany(p => pins.GetLinks(p, links)).ToList();
    }

    public bool DeleteNode(BaseNode node, TLinks links, TPins pins)
    {
        var nodePins = GetPins(node, pins);
        var nodeLinks = GetLinks(node, pins, links);

        nodeLinks.ForEach(p => links.DeleteLink(p));
        nodePins.ForEach(p => pins.DeletePin(p));

        nodes.Remove(node);
        ScriptableObject.DestroyImmediate(node, true);
        return true;	
    }

}

[Serializable]
public class TNodeComponents
{
    [SerializeField]
    public List <NodeComponent> components = new List<NodeComponent>();

    public void AddComponent(NodeComponent componant)
    {
        components.Add(componant);
    }

    public void DeleteComponents(List< NodeComponent> components)
    {
        while (components.Count > 0)
        {
            DeleteComponent(components[0]);
            components.RemoveAt(0);
        }
    }

    public void DeleteComponent(NodeComponent component)
    {
        components.Remove(component);
        ScriptableObject.DestroyImmediate(component, true);
    }
}
#endregion

[Serializable]
public class ScriptContainer : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    protected ConditionManager conditionManager;

    [SerializeField]
    protected TLinks Links;

    [SerializeField]
    protected TPins Pins;

    [SerializeField]
    public TNodes Nodes;

    [SerializeField]
    public TNodeComponents NodeComponents;

    [ClassImplements(typeof(IReflectionSystem))]
    public ClassTypeReference DialogFunctionsLibType;

    private ScriptProcessor scriptProcessor;
    private IReflectionSystem Reflection;

    #if UNITY_EDITOR
    public EditorWindow window;
    public ModelView modelView;
    #endif

    #region ISerializationCallbackReceiver implementation

    public void OnBeforeSerialize()
    {
        for (int i = 0; i < Pins.pins.Count; i++)
        {
            Pins.pins[i].PinIndex = i;
        }

        foreach (var item in Links.links)
        {
            item.Pin1Id = item.Pin1.PinIndex;
            item.Pin2Id = item.Pin2.PinIndex;
        }


        return;
    }

    public void OnAfterDeserialize()
    {
        Init();
        foreach (var item in Links.links)
        {
            item.Pin1 = Pins.pins[item.Pin1Id];
            item.Pin2 = Pins.pins[item.Pin2Id];
        }

        //TODO:Update references for all objects

    }

    #endregion

    private void Init()
    {
        object[] args = new object[1];
        args[0] = this;
        if (DialogFunctionsLibType != null)
            scriptProcessor = Activator.CreateInstance(DialogFunctionsLibType, args) as ScriptProcessor;
        else
            scriptProcessor = new ScriptProcessor(this);
    }

    public void UpdateCondition(Condition condition)
    {
        conditionManager.UpdateCondition(condition);    
    }

    public bool CheckCondition(string condition)
    {
        return conditionManager.CheckCondition(condition);
    }

    public void Exec()
    {
        conditionManager = new ConditionManager();
        scriptProcessor.Start();
    }

    public IReflectionSystem GetReflection()
    {
        return scriptProcessor.GetReflection();
    }

    public void OnEnable()
    {
        if (Nodes == null)
            Nodes = new TNodes();

        if (NodeComponents == null)
            NodeComponents = new TNodeComponents();

        if (Links == null)
            Links = new TLinks();

        if (Pins == null)
            Pins = new TPins();
        ///GetFunctionLib ().GetMethods ();
        Init();
        #if UNITY_EDITOR
        UpdateModelView();
        #endif
    }

    #if UNITY_EDITOR
    public void UpdateNodes()
    {
		
        window.Repaint();
    }

    public void LinkNodes(TPin PinA, TPin PinB)
    {
        var A = PinA.OwnerNode;
        var B = PinB.OwnerNode;
        if (A.CanBeLinkedWith(B, PinA, PinB) && B.CanBeLinkedWith(A, PinB, PinA))
        {
            TLink newLink = new TLink(PinA, PinB, this);
            Links.AddLink(newLink);
            A.Onlink(B, PinA, PinB);
            B.Onlink(A, PinB, PinA);

            UpdateModelView();
        }
    }

    public void DeleteLink(TLink link)
    {
        link.Pin1.OwnerNode.OnUnlink(link.Pin2.OwnerNode, link.Pin1, link.Pin2);
        link.Pin2.OwnerNode.OnUnlink(link.Pin1.OwnerNode, link.Pin2, link.Pin1);

        Links.DeleteLink(link);
    }

    public void UpdateModelView()
    {
        modelView = new ModelView(this);
        modelView.View.AddRange(Nodes.nodes.Cast<IEditorVisualElement>());
        modelView.View.AddRange(Links.links.Cast<IEditorVisualElement>());
        modelView.View.AddRange(Pins.pins.Cast	 <IEditorVisualElement>());
    }

    public void DeleteNode(BaseNode node)
    {
        Nodes.DeleteNode(node, Links, Pins);
        NodeComponents.DeleteComponents(node.GetOwnedComponents());
        UpdateModelView();
    }

    public BaseNode CreateNode <T>() where T: BaseNode
    {
        return Nodes.CreateNode<T>(ref Pins, NodeComponents);
    }
    #endif

    public List<TPin> GetNodePins(BaseNode node)
    {
        return Nodes.GetPins(node, Pins);
    }

    public List<BaseNode> GetConnectedNodes(BaseNode node, TPin pin)
    {
        return Nodes.GetConnectedNodes(node, Pins, Links, pin).ToList();
    }


    public BeginDialogNode GetBeginNode()
    {
        var nodes = Nodes.FindNode<BeginDialogNode>();
        if (nodes.Count == 0)
        {
            Debug.LogWarning("Dialog doesn't have any start nodes");
            return null;
        }
        return nodes[0] as BeginDialogNode;
    }




}
