﻿//
//  ScriptProcessor.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Reflection;
using System.Collections.Generic;

public partial class ScriptProcessor: IReflectionSystem
{
    protected List<MethodInfo> Methods = new List<MethodInfo>();
    protected List<EventInfo> Events = new List<EventInfo>();
    private ScriptContainer Container;
    protected ScriptContainer Script;

    public ScriptProcessor(ScriptContainer container)
    {
        Container = container;
    }

    protected virtual void ProcessNode(BaseNode node)
    {
        if (node == null)
            return;
        node.Exec(ProcessNode);
    }

    public virtual void OnStart()
    {
		
    }

    public void Start()
    {
        if (Container != null)
        {
            Script = new ScriptContainer();
            Script = Container;
            var node = Script.GetBeginNode();
            if (node != null)
                ProcessNode(node);
            OnStart();
        }	
    }

    public void ExposeMethod(string functionName)
    {
        Type type = this.GetType();
        MethodInfo methodInfo = type.GetMethod(functionName);
        if (methodInfo != null)
            Methods.Add(methodInfo);
        /*
		if (methodInfo.IsGenericMethod) {
			Type[] typeArguments = methodInfo.GetGenericArguments ();


			foreach (Type tParam in typeArguments) {
				// IsGenericParameter is true only for generic type
				// parameters.
				//
				if (tParam.IsGenericParameter) {
					if (tParam is float) {
						//VariableComponent<float> v = new VariableComponent<float> ();

					}
					//	Console.WriteLine ("\t\t{0}  parameter position {1}" +
					//	"\n\t\t   declaring method: {2}",
					//		tParam,
					//		tParam.GenericParameterPosition,
					//		tParam.DeclaringMethod);
				} else {
					Console.WriteLine ("\t\t{0}", tParam);
				}
			}
		}
		*/
        //List<VariableComponent> arguments = new List<VariableComponent> ();


        //var f = new FunctionComponent ();

    }

    public void ExposeEvent(string EventName)
    {
        Type type = this.GetType();
        EventInfo eventInfo = type.GetEvent(EventName);
        if (eventInfo != null)
            Events.Add(eventInfo);
    }

    public List<MethodInfo> GetMethods()
    {
        return Methods;
    }

    public List<EventInfo> GetEvents()
    {
        return Events;
    }

    public IReflectionSystem GetReflection()
    {
        return this as IReflectionSystem;
    }


}


