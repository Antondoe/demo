﻿//
//  Pin.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum EPinType
{
	EPinIn,
	EPinOut,
	EReroute,
}

[Serializable]
public class TPin:IEditorVisualElement
{
	public Rect PinArea;
	[SerializeField]
	public EPinType PinType;

	[SerializeField]
	private int OwnerNodeId;

	[SerializeField]
	public int PinIndex;

	[SerializeField]
	public BaseNode OwnerNode;
	//[SerializeField]
	//This field will be filled after serialization?

	[SerializeField]
	public List<TLink> Links;

	[SerializeField]
	public string PinName;

	[SerializeField]
	private Vector2 _Tangent;

	public  Vector2 Tangent {
		get{ return _Tangent; }	
	}

	#if UNITY_EDITOR
	#region ISelectable implementation

	public FDrawReply Draw (int index, Vector2 PanningDelta)
	{
		UpdatePinRect ();
		GUI.Box (PinArea, PinName);
		return FDrawReply.InputIsActive (false);
	}

	public void Select (bool bSelect)
	{
		return;
	}

	public bool IsInSelectionRect (Rect r)
	{
		//HACK
		return r.Overlaps (PinArea, true);
	}

	public void Delete ()
	{
		return;
		throw new NotImplementedException ();
	}

	public void Drag (Vector2 delta)
	{
		return;
		throw new NotImplementedException ();
	}

	public bool IsUnderMouseCursor (Vector2 mouseCoords)
	{
		return PinArea.Contains (mouseCoords);
	}

	#endregion


	private void UpdatePinRect ()
	{
		var InRect = OwnerNode.window;

		if (PinType == EPinType.EPinIn) {
			PinArea = new Rect (InRect.xMin - 20, InRect.center.y, 20, 20);
		}   
		if (PinType == EPinType.EPinOut) {
			PinArea = new Rect (InRect.xMax, InRect.center.y, 20, 20);
		}
	}
	#endif

	public TPin (BaseNode Node, EPinType pinType)
	{
            
		PinType = pinType;
		OwnerNode = Node;

		switch (pinType) {
		case EPinType.EPinIn:
			{
				PinName = "In";
				_Tangent = Vector2.left;
				break;
			}		

		case EPinType.EPinOut:
			{
				PinName = "Out";
				_Tangent = Vector2.right;
				break;
			}
					
		case EPinType.EReroute:
			{
				PinName = "Reroute";
				_Tangent = Vector2.zero;
				break;
			}		


		default:
			break;
		}

	}
};



