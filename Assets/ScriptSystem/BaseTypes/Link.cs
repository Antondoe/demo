﻿//
//  Link.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;


using System.Collections.Generic;

[Serializable]
public class TLink:IEditorVisualElement
{
	[SerializeField]
	public int Pin2Id;

	[SerializeField]
	public int Pin1Id;

	//Awailable only after deserialization
	[NonSerialized]
	public TPin Pin1;
	//Awailable only after deserialization
	[NonSerialized]
	public TPin Pin2;

	private static float TangentLength = 70;
	private bool bSelected;

	[SerializeField]
	private ScriptContainer container;

	public TLink (TPin A, TPin B, ScriptContainer container)
	{
		Pin1 = A;
		Pin2 = B;
		this.container = container;
	}
	#if UNITY_EDITOR
	#region ISelectable implementation

	public bool IsUnderMouseCursor (Vector2 mouseCoords)
	{
		var Start = Pin1.PinArea.center;
		var End = Pin2.PinArea.center;

		var distance = HandleUtility.DistancePointBezier (mouseCoords, Start, End, 
			               Start + Pin1.Tangent * TangentLength, End - Pin1.Tangent * TangentLength);
		if (distance < 10) {
			return true;
		}
				
		return false;
	}

	public void Select	(bool bSelect)
	{
		this.bSelected = bSelect;
	}

	public FDrawReply Draw (int index, Vector2 PanningDelta)
	{
		DrawLink (Pin1.PinArea.center, Pin2.PinArea.center, Pin1.Tangent, bSelected ? 5 : 2);
		return FDrawReply.InputIsActive (false);
	}

	public bool IsInSelectionRect (UnityEngine.Rect r)
	{
		//HACK! works only for rects with size smaller than 10...


		return false;
	}

	public void Delete ()
	{
		container.DeleteLink (this);
	}

	public void Drag (UnityEngine.Vector2 delta)
	{
		return;

	}

	#endregion

	

	public static void DrawLink (Vector2 Start, Vector2 End, Vector2 Tangent, float Thickness)
	{
		//HandleUtility.DistancePointBezier

		Handles.DrawBezier (Start, End, Start + Tangent * TangentLength, End - Tangent * TangentLength, Color.black, null, Thickness);
        
	}
	#endif
}

