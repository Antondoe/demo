﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class EnableNodeComponent:NodeComponent
{
    public string condition;
    #if UNITY_EDITOR
    public override FDrawReply Draw()
    {
        EditorGUIUtility.labelWidth = 55;
        EditorGUILayout.LabelField("Enable condition");
        EditorStyles.textArea.wordWrap = true;
        EditorStyles.textField.wordWrap = true;
        condition = EditorGUILayout.TextField(condition);
        return FDrawReply.InputIsActive(false);
    }
    #endif

  

}

