﻿//
//  FunctionComponent.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class EventComponent:NodeComponent /*: ISerializationCallbackReceiver*/
{
    //0 no function
    [SerializeField]
    int selectedEvent;

    [SerializeField]
    private BaseNode Owner;

    [SerializeField]
    string[] EventNames;
    Action Callback;

    //context
    private EventInfo currentEvent;
    private Delegate emittedDelegate;
    private System.Object targetObject;

    public void Initialize(BaseNode owner)
    {
        Owner = owner;
		
        var events = GetfunctionLib().GetEvents();
        UpdateEventList(events);

    }

    public void  SetCallbackAction(Action callback)
    {
        Callback = callback;
        targetObject = GetfunctionLib();
        if (targetObject == null)
            return;

        var events = (targetObject as IReflectionSystem).GetEvents();
        if (events.Count == 0)
            return;
        currentEvent = events[selectedEvent - 1];

        HookUpDelegate(currentEvent, targetObject);

    }

    private IReflectionSystem GetfunctionLib()
    {
        return Owner.container.GetReflection();
    }

    public void OnAfterDeserialize()
    {
        var lib = GetfunctionLib();
        if (lib != null)
            UpdateEventList(lib.GetEvents());
    }

    void UpdateEventList(List<EventInfo> events)
    {
        //ClearArguments ();
        if (events != null)
        {
            EventNames = new string[events.Count + 1];
            EventNames[0] = "{NoEvent}";
        }
        else
        {
            EventNames = new string[1	];
            EventNames[0] = "{NoEvent}";
            return;
        }
        for (int i = 0; i < events.Count; i++)
        {
            EventNames[i + 1] = events[i].Name;
        }
    }

    private Type GetDelegateReturnType(Type d)
    {
        if (d.BaseType != typeof(MulticastDelegate))
            throw new ApplicationException("Not a delegate.");

        MethodInfo invoke = d.GetMethod("Invoke");
        if (invoke == null)
            throw new ApplicationException("Not a delegate.");

        return invoke.ReturnType;
    }

    private List<Type> GetDelegateParametersTypesLinq(Type d)
    {
        if (d.BaseType != typeof(MulticastDelegate))
            throw new ApplicationException("Not a delegate.");

        MethodInfo invoke = d.GetMethod("Invoke");
        if (invoke == null)
            throw new ApplicationException("Not a delegate.");

        return invoke.GetParameters().Select(param => param.ParameterType).ToList();

    }

    private void HookUpDelegate(EventInfo someEvent, System.Object Obj)
    {
        Type tDelegate = someEvent.EventHandlerType;
        MethodInfo addHandler = someEvent.GetAddMethod();
        // Event handler methods can also be generated at run time,
        // using lightweight dynamic methods and Reflection.Emit. 
        // To construct an event handler, you need the return type
        // and parameter types of the delegate. These can be obtained
        // by examining the delegate's Invoke method. 
        //
        // It is not necessary to name dynamic methods, so the empty 
        // string can be used. The last argument associates the 
        // dynamic method with the current type, giving the delegate
        // access to all the public and private members of Example,
        // as if it were an instance method.
        //
        Type returnType = GetDelegateReturnType(tDelegate);
        if (returnType != typeof(void))
            throw new ApplicationException("Delegate has a return type.");

        var types = GetDelegateParametersTypesLinq(tDelegate);
        types.Insert(0, GetType()); 
        DynamicMethod handler = 
            new DynamicMethod("EventHook", 
                null,
                types.ToArray(),
                GetType());

        // Generate a method body. This method loads a string, calls 
        // the Show method overload that takes a string, pops the 
        // return value off the stack (because the handler has no
        // return type), and returns.
        //
        ILGenerator ilgen = handler.GetILGenerator(256);

        Type type = this.GetType();

        MethodInfo eventCallback = 
            type.GetMethod("OnEventFired");
        /*
        ilgen.Emit(OpCodes.Ldstr, 
            "This event handler was constructed at run time.");
            */
        ilgen.Emit(OpCodes.Ldarg_0);
        ilgen.EmitCall(OpCodes.Call, eventCallback, null);
        //ilgen.Emit (OpCodes.Call, eventCallback);
        ilgen.Emit(OpCodes.Ret);

        // Complete the dynamic method by calling its CreateDelegate
        // method. Use the "add" accessor to add the delegate to
        // the invocation list for the event.
        //
        emittedDelegate = handler.CreateDelegate(tDelegate, this);
        someEvent.AddEventHandler(Obj, emittedDelegate);
        //addHandler.Invoke (Obj, new System.Object[] { dEmitted });


    }


    public void OnEventFired()
    {

        Debug.Log("fired");
        currentEvent.RemoveEventHandler(targetObject, emittedDelegate);
        Callback();

    }


    #if UNITY_EDITOR
    public override FDrawReply Draw()
    {
        //UpdateFunctionLib ();
        EditorGUIUtility.labelWidth = 55;
        var Choice = EditorGUILayout.Popup("Event:", selectedEvent, EventNames);

        var startRect = GUILayoutUtility.GetLastRect();
        selectedEvent = Choice;

        var endRect = GUILayoutUtility.GetLastRect();

        FDrawReply reply = new FDrawReply();
        reply.rect = new Rect(startRect.xMin, startRect.yMin, endRect.xMax + endRect.xMin, endRect.yMax + endRect.yMin);

        return reply;
    }
    #endif
}