﻿//
//  FunctionComponent.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FunctionComponent:NodeComponent
{
    //0 no function
    [SerializeField]
    int selectedFunction;

    [SerializeField]
    private BaseNode Owner;

    [SerializeField]
    private List<VariableComponent> Arguments;

    [SerializeField]
    public string str;

    [SerializeField]
    string[] EventNames;

	
    public override List<NodeComponent> GetOwnedComponents()
    {
        List<NodeComponent> components = new List<NodeComponent>();
        foreach (var item in Arguments)
        {
            components.AddRange(item.GetOwnedComponents());
            components.Add(item);
        }
        return components;
    }

    /*
	public void OnBeforeSerialize ()
	{
		//Arguments = new List<VariableComponent> ();
		return;
	}

	public void OnAfterDeserialize ()
	{
		var lib = GetfunctionLib ();
		if (lib != null)
			UpdateFunctionList (lib.GetMethods ());
	}
*/
	

    public void Initialize(BaseNode owner)
    {
        Owner = owner;
        var methods = GetfunctionLib().GetMethods();
        #if UNITY_EDITOR
        UpdateFunctionList(methods);
        #endif

    }


    public void Invoke()
    {
        var lib = GetfunctionLib();
        if (lib == null)
            return;
        var methods = lib.GetMethods();
        if (selectedFunction - 1 < methods.Count)
        {

            var parameters = new object[Arguments.Count];
            for (int i = 0; i < Arguments.Count; i++)
            {
                parameters[i] = Arguments[i].GetValue()as object;	
            }

            methods[selectedFunction - 1].Invoke(lib, parameters);
        }
    }

    private IReflectionSystem GetfunctionLib()
    {
        return Owner.container.GetReflection();
    }




    private bool IsA(Type A, Type BaseType)
    {
        Type type = A.BaseType;

        while (type != null)
        {
            if (type == BaseType)
                return true;
            type = type.BaseType;
        }
        return false;
    }
    #if UNITY_EDITOR

    void UpdateFunctionList(List<MethodInfo> methods)
    {
        ClearArguments();
        if (methods != null)
        {
            EventNames = new string[methods.Count + 1];
            EventNames[0] = "{NoFunction}";
        }
        else
        {
            EventNames = new string[1	];
            EventNames[0] = "{NoFunction}";
            return;
        }
        for (int i = 0; i < methods.Count; i++)
        {
            EventNames[i + 1] = methods[i].Name;
        }
		
        if (methods.Count > 0)
        {
            SetFunction(1);
        }
        else
            SetFunction(0);
    }

    public void AddArgumet <T>(string name, Type type) where T: VariableComponent
    {
        var instance = VariableComponent.CreateInstance<T>(name, type);
		
        Owner.container.NodeComponents.AddComponent(instance);
        Arguments.Add(instance as VariableComponent);
    }

    public void ClearArguments()
    {
        if (Arguments == null)
        {
            Arguments = new List<VariableComponent>();
            return;
        }
        foreach (var item in Arguments)
        {
            Owner.container.NodeComponents.DeleteComponent(item);
        }
        Arguments.Clear();
    }

    public void SetFunction(int newFunction)
    {
        //UpdateFunctionList (methods);

        if (newFunction != selectedFunction)
        {
            ClearArguments();

            newFunction--;


            if (newFunction < 0)
            {
                Owner.UpdateNode();
                return;
            }

            var methods = GetfunctionLib().GetMethods();

            if (methods.Count == 0)
                return;

            var method = methods[newFunction];
            var ParametersInfo = method.GetParameters();
			
            foreach (var argType in ParametersInfo)
            {
                //assume that every argument is not generic
				
                if (argType.ParameterType == typeof(int))
                    AddArgumet<IntVariable>(argType.Name, typeof(int));
				
                if (argType.ParameterType == typeof(float))
                    AddArgumet<FloatVariable>(argType.Name, typeof(float));

                if (argType.ParameterType == typeof(string))
                    AddArgumet<StringVariable>(argType.Name, typeof(string));

                if (argType.ParameterType == typeof(bool))
                    AddArgumet<BoolVariable>(argType.Name, typeof(bool));


                if (IsA(argType.ParameterType, typeof(UnityEngine.Object)))
                    AddArgumet<UnityObjectVariable>(argType.Name, argType.ParameterType);
				

                //AddArgumet<VariableComponent> (argType.Name, argType.ParameterType);
                //TODO:Update method;

            }
            selectedFunction = newFunction + 1;	
            Owner.UpdateNode();
        }
    }


    public override FDrawReply Draw()
    {
        //UpdateFunctionLib ();
        EditorGUIUtility.labelWidth = 55;
        var Choice = EditorGUILayout.Popup("Function:", selectedFunction, EventNames);
        var startRect = GUILayoutUtility.GetLastRect();
        SetFunction(Choice);
        selectedFunction = Choice;

        if (Arguments != null)
            foreach (var item in Arguments)
            {
                if (item != null)
                    item.Draw();
            }

        var endRect = GUILayoutUtility.GetLastRect();

        FDrawReply reply = new FDrawReply();
        reply.rect = new Rect(startRect.xMin, startRect.yMin, endRect.xMax + endRect.xMin, endRect.yMax + endRect.yMin);

        return reply;
    }
    #endif
	
}

