﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;


public enum EVariableType
{
	typeFloat,
	typeInt,
	typeString,
	typeUnityObject,
	typeEnum,
	typeUnsupported
}


[System.Serializable]
public class VariableComponent: NodeComponent
{
	[SerializeField]
	protected string VariableName;
	[SerializeField]
	protected string variableType;

	public static T CreateInstance<T> (string VariableName, Type type) where T : VariableComponent
	{
		T res = VariableComponent.CreateInstance<T> ();
		res.VariableName = VariableName;
		res.variableType = type.AssemblyQualifiedName.ToString ();
		res.Initialize ();
		return res;
	}
	#if UNITY_EDITOR
	public virtual FDrawReply Draw ()
	{
		var reply = new FDrawReply ();
		reply.rect = GUILayoutUtility.GetLastRect ();
		return new FDrawReply ();
	}
	#endif

	public virtual object GetValue ()
	{
		return null;
	}

}


