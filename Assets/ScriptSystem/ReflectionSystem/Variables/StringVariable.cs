﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class StringVariable:VariableComponent
{
	[SerializeField]
	private string Value;
	#if UNITY_EDITOR
	public override FDrawReply Draw ()
	{
		Value = EditorGUILayout.TextField (VariableName, Value);
		var reply = new FDrawReply ();
		reply.rect = GUILayoutUtility.GetLastRect ();
		return new FDrawReply ();
	}
	#endif

	public override void Initialize ()
	{
		Value = "";
	}

	public override object GetValue ()
	{
		return Value;
	}
}
