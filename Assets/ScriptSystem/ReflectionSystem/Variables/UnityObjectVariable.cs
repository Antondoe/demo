﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class UnityObjectVariable:VariableComponent
{
	[SerializeField]
	private UnityEngine.Object Value;
	#if UNITY_EDITOR
	public override FDrawReply Draw ()
	{
		Value = EditorGUILayout.ObjectField (VariableName, Value, Type.GetType (variableType), true);
		var reply = new FDrawReply ();
		reply.rect = GUILayoutUtility.GetLastRect ();
		return new FDrawReply ();
	}
	#endif

	public override object GetValue ()
	{
		return Value;
	}
}