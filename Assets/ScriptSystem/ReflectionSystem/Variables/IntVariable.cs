﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class IntVariable:VariableComponent
{
	[SerializeField]
	private int Value;

	#if UNITY_EDITOR
	public override FDrawReply Draw ()
	{
		Value = EditorGUILayout.IntField (VariableName, Value);
		var reply = new FDrawReply ();
		reply.rect = GUILayoutUtility.GetLastRect ();
		return new FDrawReply ();
	}
	#endif

	public override void Initialize ()
	{
		Value = 0;
	}

	public override object GetValue ()
	{
		return Value;
	}
}

