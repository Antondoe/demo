﻿//
//  BoolVariable.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2016 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class BoolVariable:VariableComponent
{
	[SerializeField]
	private bool Value;

	#if UNITY_EDITOR
	public override FDrawReply Draw ()
	{

		Value = EditorGUILayout.Toggle (VariableName, Value);
		var reply = new FDrawReply ();
		reply.rect = GUILayoutUtility.GetLastRect ();
		return new FDrawReply ();
	}
	#endif

	public override void Initialize ()
	{
		Value = false;
	}

	public override object GetValue ()
	{
		return Value;
	}

}


