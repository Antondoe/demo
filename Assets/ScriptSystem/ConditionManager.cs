﻿//
//  ConditionManager.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2017 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;

//Hungarian convention.
//To Be or not to be.

public enum ConditionState
{
    NotExist,
    True,
    False,
}

//TODO: Make it nullable type?
public struct Condition
{
    public string Name;
    public ConditionState State;

    public override bool Equals(object obj)
    {
        if (obj.GetType() != typeof(Condition))
            return false;
        Condition other = (Condition)obj;
        return (other.Name == Name && other.State == State);
    }

    public bool isValid()
    {
        return State.Equals(ConditionState.NotExist);
    }

    public Condition(string condition)
    {
        var parsed = condition.Split(' ');
        int i = 0;
        Name = string.Empty;
        State = ConditionState.NotExist;
        while (i < parsed.Length - 1)
        {
            if (parsed[i] != string.Empty)
            {
                Name = parsed[i];
                break;
            }
            i++;
        }

        if (i == parsed.Length)
        {
            Name = string.Empty;
            State = ConditionState.NotExist;
            return;
        }

        while (i++ < parsed.Length - 1)
        {
            if (parsed[i] != string.Empty)
            {
                State = (ConditionState)Enum.Parse(typeof(ConditionState), parsed[i], true);
                return;
            }
            i++;
        }
    }
}

public class ConditionManager
{
    private List<Condition> conditions = new List<Condition>();

    private static string RemoveWhitespace(string input)
    {
        if (input == string.Empty)
            return input;

        return new string(input.ToCharArray()
        .Where(c => !Char.IsWhiteSpace(c))
        .ToArray());
    }

    Condition? GetCondition(string name)
    {
        name = name.ToLower();
        return conditions.Find(p => p.Name.ToLower() == name);
    }

    public void UpdateCondition(Condition condition)
    {
        int index = conditions.FindIndex(p => p.Name == condition.Name);
        if (index >= 0)
            conditions[index] = condition;
        else
            conditions.Add(condition);    
    }

    /// <summary>
    /// Checks the condition.
    /// </summary>
    /// <returns><c>true</c>, if every condition is exist and true, <c>false</c> otherwise.</returns>
    /// <param name="Condition">Condition.</param>
    public bool CheckCondition(string Condition)
    {
        //maybe check if we are waiting for Not Exist;

        var parsedArray = new List<string>(Condition.Split(';'));

      
        string Element = string.Empty;

        if (parsedArray.Count == 0)
            return false;
      
        while (parsedArray.Count > 0)
        {
            Element = parsedArray[0];
            if (Element != string.Empty)
            {
                var cnd = new Condition(Element);
                int index = conditions.FindIndex(p => p.Name == cnd.Name);
                
                if (index > -1)
                {
                    if (conditions[index].Equals(cnd) == false)
                        return false; 
                }
                else if (cnd.State != ConditionState.NotExist)
                    return false;

            }
           
            parsedArray.RemoveAt(0);
            
        }

        return true;
    }
    
}

