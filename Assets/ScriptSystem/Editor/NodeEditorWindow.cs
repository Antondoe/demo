using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEditor;
using System.Linq;
using VNKit;




public class NodeEditorWindow : EditorWindow
{
    [SerializeField]
    private ScriptContainer Script;

    private Vector2 mousePosition;
    private Event lastEvent = new Event();
    private bool bPanning = false;
    //zooming
    private float zoomLevels = 20;
    [SerializeField]
    private float zoom = 20;
    private Rect backgroundRect = new Rect(0, 0, 4096, 4096);
    //---------------------------------------------------

    #region Working with Assets and registring new window

    [MenuItem("Window/Node Editor")]
    private static void Init()
    {
        OpenAsset(Selection.activeObject);        
    }

    public static void OpenAsset(UnityEngine.Object AssetObject)
    {
        NodeEditorWindow w = GetWindow<NodeEditorWindow>(); 
        var openDialog = AssetObject as ScriptContainer;
        if (openDialog != null)
        {
            w.Script = openDialog;
            w.Script.window = w;
            w.Script.UpdateModelView();
        }
    }

    [UnityEditor.Callbacks.OnOpenAsset(1)]
    public static bool OnOpenAsset(int instanceID, int line)
    {
        ScriptContainer openDialog = Selection.activeObject as ScriptContainer;
        if (openDialog != null)
        {
            OpenAsset(openDialog);
        }        

        return false; // let unity open the file
    }

    public static string GetSelectedPath()
    {
        string path = "Assets";

        foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
        {
            path = AssetDatabase.GetAssetPath(obj);
            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                path = Path.GetDirectoryName(path);
                break;
            }
        }
        return path;
    }

    public static string GenerateAssetUnicName(string Path, string Name)
    {
        var asset = AssetDatabase.LoadAssetAtPath <UnityEngine.Object>(Path + "/" + Name + ".asset");
        if (asset != null)
        {
            var resultString = Regex.Match(Name, @"\d+").Value;
            int index = 0;
            Int32.TryParse(resultString, out index);
            index++;
            return GenerateAssetUnicName(Path, Name + index);
        }
        else
            return Name;
    }

    [MenuItem("Assets/Create/New Dialog")]
    static void CreateAsset()
    {
        ScriptContainer Dialog = CreateInstance<ScriptContainer>();
        var path = GetSelectedPath();
        var AssetName = GenerateAssetUnicName(path, "NewDialog");
        Debug.Log(path);
        AssetDatabase.CreateAsset(Dialog, path + "/" + AssetName + ".asset");
    }

    #endregion

    void Awake()
    {
        if (Script != null)
            Script.window = this;
        Repaint();
    }

    void OnEnable()
    {
        if (Script == null)
            Script = CreateInstance<ScriptContainer>();
    }

    Vector2 GetZoomedCoordinates(Vector2 MousePosition)
    {
        return MousePosition * zoomLevels / zoom;
    }

    void AddObject(ScriptableObject newAsset)
    {
        AssetDatabase.AddObjectToAsset(newAsset, Script);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newAsset));
    }

    void AddObjectss(List<ScriptableObject> objects)
    {
        if (objects.Count == 0)
            return;

        foreach (var item in objects)
        {
            AssetDatabase.AddObjectToAsset(item, Script);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(item));
        }
    }

    void CreateNewNode<T>() where T : BaseNode
    {
        if (Script == null)
        {
            return;
        }

        List<ScriptableObject> newAssets = new List<ScriptableObject>();
        BaseNode newNode = Script.CreateNode<T>();
        var Position = mousePosition;
        newNode.window.center = new Vector2(Position.x, Position.y);
        newNode.container = Script;
        newNode.Init();
        newAssets.Add(newNode);
        var nodeComponents = newNode.GetOwnedComponents().Cast<ScriptableObject>();
        newAssets.AddRange(nodeComponents);
        AddObjectss(newAssets);
        AssetDatabase.SaveAssets();
        Script.UpdateModelView();
    }


    void DrawContextMenu()
    {
        Event e = Event.current;
        GenericMenu menu = new GenericMenu();

        menu.AddItem(new GUIContent("Add Reroute"), false, CreateNewNode<RerouteNode>);
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Add Begin Node"), false, CreateNewNode<BeginDialogNode>);
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Add Update Condition Node"), false, CreateNewNode<UpdateConditionNode>);
        menu.AddItem(new GUIContent("Add Dialog Node"), false, CreateNewNode<DialogNode>);
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Add Switch Dialog Flow Node"), false, CreateNewNode<SwitchDialogFlowNode>);
        menu.AddItem(new GUIContent("Add Switch Dialog Flow Option Node"), false, CreateNewNode<SwitchFlowOptionNode>);
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Invoke Function"), false, CreateNewNode<InvokeEventNode>);
        menu.AddItem(new GUIContent("Wait for Event"), false, CreateNewNode<WaitForEventNode>);
        menu.AddSeparator("");
        menu.AddItem(new GUIContent("Play Animation"), false, CreateNewNode<PlayAnimimationNode>);
        menu.ShowAsContext();
        e.Use();

    }

    void SaveScript()
    {
        foreach (var item in Script.NodeComponents.components)
        {
            if (!AssetDatabase.IsSubAsset(item))
                AssetDatabase.AddObjectToAsset(item, Script);
        }

        EditorUtility.SetDirty(Script);
        AssetDatabase.SaveAssets();
    }

    void DrawBackground()
    {
        EditorGUI.DrawRect(backgroundRect, Color.gray);
    }

    void DrawNodes()
    {
        DrawBackground();
        if (GUI.Button(new Rect(0, 0, 40, 20), "Save"))
        {
            SaveScript();
        }
        EditorZoomArea.Begin(zoom / zoomLevels, new Rect(0, 0, 9999, 9999));
        BeginWindows();
        Script.modelView.Draw();
        EndWindows();
        EditorZoomArea.End();
    }


    #region Event Handling

    void OnMouseUp(Vector2 MouseCoords, int button)
    {
        if (button == 1 && !bPanning)
            DrawContextMenu();

        Script.modelView.OnMouseUp(MouseCoords, button);
        bPanning = false;
        Repaint();
        return;
    }

    void OnMouseDown(Vector2 MouseCoords, int button)
    {

        Script.modelView.OnMouseDown(MouseCoords, button);

    }

    void OnMouseDrag(Vector2 MouseCoords, Vector2 delta, int button)
    {
        if (button == 1)
        {
            Script.modelView.Pan(delta);
            Repaint();
            bPanning = true;
            return;
        }
        Script.modelView.OnDrag(MouseCoords, delta);

        Repaint();
        return;
    }

    void OnGUI()
    {
        DrawNodes(); 
        if (Script == null)
        {
            Debug.Log("Dialog is null");
            return;
        }
     
        lastEvent = Event.current;
        //Debug.Log(lastEvent.type);
        mousePosition = GetZoomedCoordinates(lastEvent.mousePosition);

        if ((lastEvent.type == EventType.mouseDown))
        {

            OnMouseDown(mousePosition, lastEvent.button);
        }

        if ((lastEvent.type == EventType.mouseUp))
        {
            OnMouseUp(mousePosition, lastEvent.button);

        }

        //Drag nodes    
        if (lastEvent.type == EventType.mouseDrag)
        {    
            OnMouseDrag(mousePosition, GetZoomedCoordinates(lastEvent.delta), lastEvent.button);	
        }

        if (lastEvent.keyCode == KeyCode.Delete)
        {
            Script.modelView.DeleteSelected();
            Repaint();

        }

        if ((lastEvent.type == EventType.scrollWheel))
        {
            zoom += lastEvent.delta.y;
            zoom = Mathf.Clamp(zoom, 8, zoomLevels);

            Repaint();	
        }
    }

    #endregion
}
