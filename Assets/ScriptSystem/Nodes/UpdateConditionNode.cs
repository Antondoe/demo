﻿//
//  UpdateConditionNode.cs
//
//  Author:
//       kid <>
//
//  Copyright (c) 2017 kid
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
[Serializable]
public class UpdateConditionNode:BaseNode
{
    [SerializeField]
    private string condition = "";

    public override void Exec(Action<BaseNode> Callback)
    {
        var conditions = condition.Split(';');
        int i = 0;
        while (i < conditions.Length)
        {
            string word = conditions[i];
            //TODO: check if condition is properly set; 
            if (word != string.Empty)
                container.UpdateCondition(new Condition(conditions[i]));
            i++;
        }

        base.Exec(Callback);
    }

    #if UNITY_EDITOR
    public UpdateConditionNode()
    {
        window = new Rect(0, 0, 200, 20);
    }

    protected override FDrawReply OnGUICustom(int Handle)
    {
        EditorStyles.textArea.wordWrap = true;
        EditorStyles.textField.wordWrap = true;
        condition = EditorGUILayout.TextArea(condition);

        return base.OnGUICustom(Handle);
    }
    #endif
}

