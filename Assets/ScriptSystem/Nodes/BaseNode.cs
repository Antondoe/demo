using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


public struct FDrawReply
{
    public bool bInputIsActive;
    public Rect rect;

    public static FDrawReply InputIsActive(bool bActive)
    {
        var r = new FDrawReply();
        r.bInputIsActive = bActive;
        return r;
    }


}

[Serializable]
public class BaseNode : ScriptableObject, IEditorVisualElement
{
    #if UNITY_EDITOR
    public EditorWindow editorWindow;

    [SerializeField]
    protected float width;

    [SerializeField]
    protected float height;

    [SerializeField]
    public Rect	window;

    protected bool bInputIsActive = false;
    protected bool bSelected;
    #endif

    [SerializeField]
    public ScriptContainer container;







    public BaseNode()
    {
        #if UNITY_EDITOR
        window = new Rect(0, 0, width, height);
        #endif
    }

    public virtual void OnEnable()
    {
        hideFlags = HideFlags.HideInHierarchy;
    }

    /// <summary>
    /// Init this instance.
    /// </summary>
    public virtual void Init()
    {
        return;
    }

    public virtual List<TPin> GeneratePins()
    {
        List< TPin > pins = new List<TPin>();
        pins.Add(new TPin(this, EPinType.EPinIn));
        pins.Add(new TPin(this, EPinType.EPinOut));
        return pins;
    }

    public virtual List<NodeComponent> GetOwnedComponents()
    {
        return new List<NodeComponent>();
    }

    /// <summary>
    /// Determines whether this node is enabled and can be executed
    /// </summary>
    /// <returns><c>true</c> if this instance is enabled; otherwise, <c>false</c>.</returns>
    public virtual bool IsNodeEnabled()
    {
        return true;
    }

    public virtual void Exec(Action<BaseNode> Callback)
    {
        if (IsNodeEnabled())
            Callback(GetNextNode());
    }

    protected virtual BaseNode GetNextNode()
    {

        var pins = container.GetNodePins(this);
        if (pins.Count == 0)
            return null;
        var pin = pins.Find(p => p.PinType == EPinType.EPinOut);
        var nodes = container.GetConnectedNodes(this, pin);
        if (nodes.Count == 0)
            return null;
        else
        {
            var node = nodes.Find(p => p.IsNodeEnabled());
            return node;
        }
    }


    #if UNITY_EDITOR
    #region ISelectable implementation

    public bool IsUnderMouseCursor(Vector2 mouseCoords)
    {
        return window.Contains(mouseCoords);
    }

    public FDrawReply Draw(int Handle, Vector2 PanningDelta)
    {
        DrawNode(Handle, PanningDelta);
        return FDrawReply.InputIsActive(false);
    }

    public void Select(bool bSelect)
    {
        bSelected = bSelect;
        //Change visual style somehow

    }

    public void Drag(Vector2 delta)
    {
        window.center += delta;

    }

    public bool IsInSelectionRect(Rect r)
    {
        return r.Overlaps(window, true);
    }

    public virtual void Delete()
    {
        if (!bInputIsActive)
            container.DeleteNode(this);
    }

    #endregion

    public virtual bool CanBeLinkedWith(BaseNode TargetNode, TPin OwnedPin, TPin TargetPin)
    {
        return true;
    }

    public virtual void OnUnlink(BaseNode Target, TPin OwnedPin, TPin TargetPin)
    {
        return;
    }

    public virtual void Onlink(BaseNode Target, TPin OwnedPin, TPin TargetPin)
    {
        return;
    }

    public void UpdateNode()
    {
        if (editorWindow != null)
            editorWindow.Repaint();
    }

    public Rect DrawNode(int Handle, Vector2 PanningDelta)
    {
        Rect wnd = window;
        wnd.position += PanningDelta;
        GUIStyle style = new GUIStyle(GUI.skin.window);

        if (bSelected)
            style.normal = style.onNormal;

        window = GUI.Window(Handle, wnd, _OnGUI, "Node", style);
		
        return wnd;
    }

    protected virtual FDrawReply OnGUICustom(int Handle)
    {
        return FDrawReply.InputIsActive(true);
    }

    protected void UpdateNodes()
    {
        editorWindow.Repaint();
    }

    protected void _OnGUI(int Handle)
    {
        GUILayout.Space(10);
        GUILayoutUtility.GetLastRect();
        var reply = OnGUICustom(Handle);
        EditorGUILayout.Separator();
        var endRect = GUILayoutUtility.GetLastRect();
        //reply.rect = new Rect(startRect.xMin, startRect.yMin, endRect.xMax + endRect.xMin, endRect.yMax + endRect.yMin);
        if (Event.current.type == EventType.Repaint)
            window.height = endRect.yMax;
        PostDraw(Handle);
        var currentEvent = Event.current; 

        if (currentEvent.keyCode == KeyCode.Delete && !bInputIsActive && window.Contains(currentEvent.mousePosition + window.position))
        {
            //Delete();
        }

        if (currentEvent.button == 1 && window.Contains(currentEvent.mousePosition + window.position))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Remove Node"), false, Delete);
            menu.ShowAsContext();
            currentEvent.Use();
        }
    }

    private void PostDraw(int Handle)
    {
        //GUI.DragWindow ();
    }
    #endif
    //Internal logic


}




