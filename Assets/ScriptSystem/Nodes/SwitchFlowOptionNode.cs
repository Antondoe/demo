﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;



[Serializable]
public class SwitchFlowOptionNode : BaseDialogNode
{
    [SerializeField]
    public string Text;
    [SerializeField]
    private SwitchDialogFlowNode parent;
   

    #if UNITY_EDITOR

    public override void Onlink(BaseNode Target, TPin OwnedPin, TPin TargetPin)
    {
        if (OwnedPin.PinType == EPinType.EPinIn)
            parent = Target as SwitchDialogFlowNode;
    }

    public override void OnUnlink(BaseNode Target, TPin OwnedPin, TPin TargetPin)
    {
        if (OwnedPin.PinType == EPinType.EPinIn)
            parent = null;
    }

    public SwitchFlowOptionNode()
    {
        window = new Rect(0, 0, 200, 100);
    }

    protected override FDrawReply OnGUICustom(int Handle)
    {
        Text = EditorGUILayout.TextArea(Text);

        if (Event.current.type == EventType.MouseUp)
        {
            var R = GUILayoutUtility.GetLastRect();
            if (!R.Contains(Event.current.mousePosition))
            {
                //GUI.FocusControl("");
                bInputIsActive = false;
                    
            }
            else
            {
                //bInputIsActive = true;
            }
        }

        base.OnGUICustom(Handle);
     
        return FDrawReply.InputIsActive(false);
    }
    #endif

    public override ESpeakerType GetSpeakerType()
    {
        if (parent != null)
            return (parent as SwitchDialogFlowNode).SpeakerType;
		
        return ESpeakerType.NA;
    }

    public override string GetDialogText()
    {
        return Text;
    }

}
