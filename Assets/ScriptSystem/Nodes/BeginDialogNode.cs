﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;



[Serializable]
public class BeginDialogNode : BaseNode
{
	#if UNITY_EDITOR
	public BeginDialogNode ()
	{
		window = new Rect (0, 0, 100, 50);
	}

	protected override FDrawReply OnGUICustom (int Handle)
	{
		EditorGUILayout.LabelField ("Start Dialog");
		return FDrawReply.InputIsActive (false);
	}
	#endif
}