﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;






[System.Serializable]
public class InvokeEventNode:BaseNode
{
    [SerializeField]
    public FunctionComponent function;

    public Action <BaseNode> callback;

    public InvokeEventNode()
    {
        #if UNITY_EDITOR
        width = 200;
        height = 50;
        window = new Rect(0, 0, width, height);
        #endif
    }

    public override void OnEnable()
    {
        base.OnEnable();
        //if (function != null)
        //	function.UpdateFunctionLib ();

    }

    public override void Init()
    {
        base.Init();

        function = ScriptableObject.CreateInstance<FunctionComponent>();
        function.Initialize(this);
        //container.CreateComponent(this);
    }

    public override List<NodeComponent> GetOwnedComponents()
    {
        var ownedComponents = function.GetOwnedComponents();
        ownedComponents.Add(function);
        return ownedComponents;
    }

    public override void Exec(Action<BaseNode> Callback)
    {
        function.Invoke();
        Callback(GetNextNode());
    }

    protected override BaseNode GetNextNode()
    {
        return base.GetNextNode();
    }
    #if UNITY_EDITOR
    public override void Delete()
    {
        function.ClearArguments();
        base.Delete();
    }

    protected override FDrawReply OnGUICustom(int Handle)
    {
        //return base.OnGUICustom (Handle);
        //someVariable.Draw ();
        //return FDrawReply.InputIsActive (false);
			 
        if (function != null)
        {
            var reply = function.Draw();
            /*	if (Event.current.type == EventType.repaint)
				window.height = reply.rect.height / 2 + reply.rect.yMin;
		*/	//window.width = width + reply.rect.width + 10;	
        }
        return FDrawReply.InputIsActive(false);
    }
    #endif
}

