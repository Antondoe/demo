using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


public enum ESpeakerType
{
    Player,
    NPC,
    Subtitles,
    NA
}



[Serializable]
public class BaseDialogNode : BaseNode
{
    [SerializeField]
    bool bEnterOnlyOnce;

    [SerializeField]
    private string condition;

    private bool bExecuted = false;

    public override void OnEnable()
    {
        base.OnEnable();
        bExecuted = false;
    }

    public override void Init()
    {
        base.Init();
        bEnterOnlyOnce = false;
    }

    public virtual string GetDialogText()
    {
        return string.Empty;
    }

    public override bool IsNodeEnabled()
    {
        return !(bExecuted && bEnterOnlyOnce) && container.CheckCondition(condition);
    }

    #if UNITY_EDITOR

    protected override FDrawReply OnGUICustom(int Handle)
    {
        EditorGUILayout.LabelField("Check Conditions:");
        condition = EditorGUILayout.TextArea(condition);
        EditorGUIUtility.labelWidth = 100;
        bEnterOnlyOnce = EditorGUILayout.ToggleLeft("EnterOnce", bEnterOnlyOnce);
        return base.OnGUICustom(Handle);
    }

    public override void OnUnlink(BaseNode Target, TPin OwnedPin, TPin TargetPin)
    {
        base.OnUnlink(Target, OwnedPin, TargetPin);
    }
      
    #endif

    public virtual ESpeakerType GetSpeakerType()
    {
        return ESpeakerType.NA;
    }

    public override void Exec(Action<BaseNode> Callback)
    {
        bExecuted = true;
        Callback(GetNextNode());
    }

}

