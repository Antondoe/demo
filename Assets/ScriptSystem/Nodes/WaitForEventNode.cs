﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


[System.Serializable]
public class WaitForEventNode:BaseNode
{

    [SerializeField]
    EventComponent eventConponent;

    private Action<BaseNode> Callback;
    #if UNITY_EDITOR
    public WaitForEventNode()
    {
        width = 200;
        height = 50;
        window = new Rect(0, 0, width, height);
    }
    #endif
    public override void Init()
    {
        base.Init();

        eventConponent = NodeComponent.CreateInstance<EventComponent>();
        eventConponent.Initialize(this);
    }

    public override List<NodeComponent> GetOwnedComponents()
    {
        var components = eventConponent.GetOwnedComponents();
        components.Add(eventConponent);
        return components;
    }

    public void OnEventFired()
    {
        Callback(GetNextNode());
        Debug.Log("Node exec");
    }

    public override void Exec(Action<BaseNode> Callback)
    {
        this.Callback = Callback;
        if (eventConponent != null)
            eventConponent.SetCallbackAction(OnEventFired);
    }
    #if UNITY_EDITOR
    protected override FDrawReply OnGUICustom(int Handle)
    {
        //return base.OnGUICustom (Handle);
        //someVariable.Draw ();
        //return FDrawReply.InputIsActive (false);
        	 
        if (eventConponent != null)
        {
            var reply = eventConponent.Draw();
            ////window.width = width + reply.rect.width + 10;	
        }
        
        return FDrawReply.InputIsActive(false);
    }
    #endif

}

