﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.Experimental.Director;

[Serializable]
public class PlayAnimimationNode:BaseNode
{
	[SerializeField]
	public AnimationClip animation;

	[SerializeField]
	public string ObjectName;

	[SerializeField]
	public bool bWaitForEnd;

	private AnimationEvent animEvent;
	private AnimationController AnimController;
	private Action <BaseNode> callback;

	#if UNITY_EDITOR
	public PlayAnimimationNode ()
	{
		width = 250;
		height = 90;
		window = new Rect (0, 0, width, height);
	}

	protected override FDrawReply OnGUICustom (int Handle)
	{
		EditorGUIUtility.labelWidth = 100;
		bWaitForEnd = EditorGUILayout.Toggle ("Wait for end", bWaitForEnd);
		ObjectName = EditorGUILayout.TextField ("Object Name", ObjectName);
		animation = (AnimationClip)EditorGUILayout.ObjectField ("Animation Clip", animation, typeof(AnimationClip), false);
		return FDrawReply.InputIsActive (true);
	}

	#endif

	public override void OnEnable ()
	{
		base.OnEnable ();
		if (ObjectName == null) {
			ObjectName = "";
		}
	}

	private AnimationEvent GenerateEvent (GameObject targetObject)
	{
		AnimationEvent e = new AnimationEvent ();
		e.time = animation.length;
		e.functionName = "OnAnimationFinished";
		e.stringParameter = animation.name;

		foreach (var item in animation.events) {
			if (e.functionName == item.functionName &&
			    e.time == item.time &&
			    e.stringParameter == item.stringParameter &&
			    e.intParameter == item.intParameter &&
			    e.floatParameter == item.floatParameter &&
			    e.objectReferenceParameter == item.objectReferenceParameter) {
				return item;
			
			}
		}

		
		animation.AddEvent (e);
		return e;
	}

	private void OnAnimationFinished ()
	{
		Destroy (AnimController);
		callback (GetNextNode ());
	}

	public override void Exec (Action<BaseNode> Callback)
	{
		SceneConfig sceneConfig = SceneConfig.GetCurrentSceneConfig ();
		var targetObject = sceneConfig.GetObjectByName (ObjectName) as GameObject;

		if (targetObject == null) {
			Debug.Log ("Can't play animation for object " + ObjectName + " Object doesn't exist.");
			return;
		}

		if (animation == null) {
			Debug.Log ("Animation not set for object " + ObjectName);
		}

		var controller = targetObject.GetComponent<Animator> ();
		var clipPlayable = AnimationClipPlayable.Create (animation);
		

		if (bWaitForEnd) {
			animEvent = GenerateEvent (targetObject);	
			AnimController = targetObject.AddComponent<AnimationController> ();
			callback = Callback;
			AnimController.OnFinished = OnAnimationFinished;
			controller.Play (clipPlayable);	
		} else {
			controller.Play (clipPlayable);	
			Callback (GetNextNode ());
		}
				
	
	}

}


