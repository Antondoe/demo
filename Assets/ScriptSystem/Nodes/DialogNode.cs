﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class DialogNode:BaseDialogNode
{
    [SerializeField]
    public string
        Text;

    [SerializeField]
    public ESpeakerType SpeakerType;

    public DialogNode()
    {
        #if UNITY_EDITOR
        window = new Rect(0, 0, 200, 200);
        #endif
    }

    public override ESpeakerType GetSpeakerType()
    {
        return SpeakerType;
    }

    public override string GetDialogText()
    {
        return Text;
    }
    #if UNITY_EDITOR

    protected override FDrawReply OnGUICustom(int WindowHandle)
    {
        //EditorGUILayout.BeginVertical("Text");
        //window = EditorGUILayout.RectField (window);
        SpeakerType = (ESpeakerType)EditorGUILayout.EnumPopup("Speaker", SpeakerType);
        EditorStyles.textArea.wordWrap = true;
        EditorStyles.textField.wordWrap = true;
        Text = EditorGUILayout.TextArea(Text);
        // Debug.Log(Event.current.type);
        if (Event.current.isMouse)
        {
            var R = GUILayoutUtility.GetLastRect();
            if (!R.Contains(Event.current.mousePosition))
            {
                //GUI.FocusControl("");
                bInputIsActive = false;
                    
            }
            else
            {
                bInputIsActive = true;
            }
        }
        base.OnGUICustom(WindowHandle);


        return FDrawReply.InputIsActive(true);

      
    }
    #endif
}


