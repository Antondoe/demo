﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

/// <summary>
/// Sequentional - draws first node that is enabled
/// Random - Draw random node
/// UserSelected - draws list of all nodes where next node can be picked by player
/// </summary>
public enum ESwitchType
{
    Sequientional,
    Random,
    UserSelected
}

[Serializable]
public class SwitchDialogFlowNode : BaseNode
{
    [SerializeField]
    public ESpeakerType SpeakerType;
	
    [SerializeField]
    public ESwitchType SwitchType;

    //Aftrer deserialization
    public TPin OutPin;

    private BaseNode NextNode;

    private int LastIndex = 0;
    private int UserSelectedIndex = 0;
    #if UNITY_EDITOR
    public SwitchDialogFlowNode()
    {
        window = new Rect(0, 0, 200, 100);
    }

    protected override FDrawReply OnGUICustom(int Handle)
    {
        EditorGUILayout.LabelField("Switch Dialog Flow");
        SpeakerType = (ESpeakerType)EditorGUILayout.EnumPopup("Speaker", SpeakerType);
        SwitchType = (ESwitchType)EditorGUILayout.EnumPopup("Selection type", SwitchType);
        return FDrawReply.InputIsActive(false);
    }
    #endif
    public void SetNextNode(int NodeIndex)
    {
        UserSelectedIndex = NodeIndex;
    }

    protected override BaseNode GetNextNode()
    {
        int nextNodeIndex = -1;

        var Childs = container.GetConnectedNodes(this, OutPin);

        switch (SwitchType)
        {
            case ESwitchType.Random:
                {
                    nextNodeIndex =	UnityEngine.Random.Range(0, Childs.Count - 1);
                    break;
                }

            case ESwitchType.Sequientional:
                {
                    nextNodeIndex = LastIndex++;
                    break;
                }

            case ESwitchType.UserSelected:
                {
                    nextNodeIndex = UserSelectedIndex;
                    break;
                }	
            default:
                break;
        }	

        if (nextNodeIndex < Childs.Count && nextNodeIndex > 0)
            return Childs[nextNodeIndex];	

        return null;
    }


}


