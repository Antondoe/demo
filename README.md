# README #



### This is demonstration of my node-based visual script editor for unity ###

* This Git contains a Unity Project, that demonstates the current version of my Visual script editor
* Version: Pre-alfa
* [Learn more](http://antondoe.blogspot.ru/2017/02/node-based-visual-dialog-editor-for.html)

### Features ###
* Function calls.
You can implement any function in C# and call it in your script. It supports various arguments. Currently, only string, float. int, bool and Unity.Object types of args are supported. On a sample you can see, that function "ShowWindow" is called with argument "NameForm". In order to be invoked, function must be exposed on C# side by calling a method. After this, function will be available from any dialog asset.

* Event Bindings.
Bind to event declared on c# side as event without any parameters.

* Branching.
It supports branching. You can see that dialog has three options to continue. Node "Switch Dialog Flow" let you to create branches of options. Selection type:"User Selected" means, that every option after Switch node will be displayed on a screen, and after player makes a choice, dialog continues.
  
* Conditions.
You can create and check conditions inside dialogues. Every dialogue node has a field where you can write some expression to check. Expression is formed from conditions. Condition has its name and state: true, false, notexisted. On example below, there is only one condition:door_closed. It is created for a first time with its state set to false.

### Set-up ###

* Unity 5+ is required to run this project